package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testInputFrameInAGame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertTrue(game.getFrameAt(2).equals(new Frame(7, 2)));
	}
	
	@Test
	public void testScoreOfAGame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void testScoreOfAGameWithSpareFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void testScoreOfAGameWithStrikeFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void testScoreOfAGameWithStrikeFrameFollowedBySpareFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void secondTestScoreOfAGameWithStrikeFrameFollowedBySpareFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(101, game.calculateScore());
	}
	
	@Test
	public void testScoreOfAGameWithStrikeFrameFollowedByStrikeFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(112, game.calculateScore());
	}

	@Test
	public void testScoreOfAGameWithSpareFrameFollowedBySpareFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void secondTestScoreOfAGameWithSpareFrameFollowedBySpareFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(7, 3));
		game.addFrame(new Frame(9, 1));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(99, game.calculateScore());
	}
	
	@Test
	public void testInputFirstBonusThrow() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		// It sets the first bonus throw.
		game.setFirstBonusThrow(7);
		
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void testScoreOfAGameWithSpareFrameAsTheLastFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		// It sets the first bonus throw.
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testInputSecondBonusThrow() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		// It sets the first bonus throw.
		game.setFirstBonusThrow(7);
		// It sets the second bonus throw.
		game.setSecondBonusThrow(2);

		assertEquals(2, game.getSecondBonusThrow());
	}
	
	@Test
	public void testScoreOfAGameWithStrikeFrameAsTheLastFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		// It sets the first bonus throw.
		game.setFirstBonusThrow(7);
		// It sets the second bonus throw.
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testScoreOfAGameWithAllStrikeFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		// It sets the first bonus throw.
		game.setFirstBonusThrow(10);
		// It sets the second bonus throw.
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}
	
	@Test
	public void testNumberOfFramesInAGame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(10, game.getSize());
	}
	
	@Test
	public void secondTestNumberOfFramesInAGame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 8 frames to this bowling game.
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		
		assertEquals(8, game.getSize());
	}

	@Test
	public void testScoreOfAGameWithMultipleStrikesFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(105, game.calculateScore());
	}
	
	@Test
	public void testScoreOfAGameWithMultipleSpares() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(93, game.calculateScore());
	}
	
	@Test (expected = BowlingException.class)
	public void testIndexForGetFrameAtGreaterThan10() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		game.getFrameAt(11);
	}
	
	@Test (expected = BowlingException.class)
	public void testGameWithMoreThan10Frames() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 11 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		game.addFrame(new Frame(4, 6));
	}
	
	@Test 
	public void testScoreOfAGameWithLessThe10Frames() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 8 frames to this bowling game.
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(69, game.calculateScore());
	}
	
	@Test (expected = BowlingException.class)
	public void testInvalidInputFirstBonusThrow() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		// It sets the first bonus throw.
		game.setFirstBonusThrow(71);
		// It sets the second bonus throw.
		game.setSecondBonusThrow(2);
	}
	
	@Test (expected = BowlingException.class)
	public void testInvalidInputSecondBonusThrow() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		// It sets the first bonus throw.
		game.setFirstBonusThrow(7);
		// It sets the second bonus throw.
		game.setSecondBonusThrow(21);
	}
}

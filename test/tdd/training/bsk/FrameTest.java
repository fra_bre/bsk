package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void testInputFirstThrow() throws Exception{
		Frame frame = new Frame (2, 4);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void testInputSecondThrow() throws Exception{
		Frame frame = new Frame (2, 4);
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test
	public void testScoreOfAFrame() throws Exception{
		Frame frame = new Frame (2, 6);
		assertEquals(8, frame.getScore());
	}

	@Test
	public void testInputBonusForAFrame() throws Exception{
		Frame frame = new Frame (2, 6);
		frame.setBonus(4);
		assertEquals(4, frame.getBonus());
	}
	
	@Test
	public void testFrameShouldBeSpare() throws Exception{
		Frame frame = new Frame (4, 6);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testScoreOfASpareFrame() throws Exception{
		Frame frame = new Frame (4, 6);
		frame.setBonus(4);
		assertEquals(14, frame.getScore());
	}
	
	@Test
	public void testFrameShouldBeStrike() throws Exception{
		Frame frame = new Frame (10, 0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testScoreOfAStrikeFrame() throws Exception{
		Frame frame = new Frame (10, 0);
		frame.setBonus(8);
		assertEquals(18, frame.getScore());
	}
	
	@Test(expected = BowlingException.class)
	public void testScoreOfAFrameGreaterThan10() throws Exception{
		Frame frame = new Frame (10, 1);
	}
	
	@Test(expected = BowlingException.class)
	public void testScoreOfAFrameLessThan0() throws Exception{
		Frame frame = new Frame (-1, 0);
	}
}

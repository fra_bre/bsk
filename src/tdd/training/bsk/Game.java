package tdd.training.bsk;

public class Game {
	
	private Frame[] framesOfAGame;
	private static final int DIMFRAMESOFAGAME = 10;
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		framesOfAGame = new Frame [DIMFRAMESOFAGAME];
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(getSize() > 9)
			throw new BowlingException ("A single game can have maximum 10 frames");
		framesOfAGame[getSize()] = frame;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index < 0 || index > 9)
			throw new BowlingException ("The index of Frames has as range the interval [0,9]");
		
		return framesOfAGame[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if(firstBonusThrow < 0 || firstBonusThrow > 10)
			throw new BowlingException ("The first bonus has as range the interval [0,10]");
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(secondBonusThrow < 0 || secondBonusThrow > 10)
			throw new BowlingException ("The second bonus has as range the interval [0,10]");
		
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int totalScore = 0;
		
		for(int i = 0; i < getSize(); i++) {
			if(framesOfAGame[i].isSpare()) {
				setBonusForSpareFrame(i);
			}else if(framesOfAGame[i].isStrike()) {
				setBonusForStrikeFrame(i);
			}
				
			totalScore += framesOfAGame[i].getScore();
		}
		return totalScore;	
	}

	/**
	 * It returns the number of frames of this game up to that point
	 * 
	 * @return The number of frames of this game
	 */
	public int getSize() {
		int counterOfFrames = 0;
		
		for(int i = 0; i < DIMFRAMESOFAGAME && framesOfAGame[i] != null; i++) {
			counterOfFrames++;
		}
		return counterOfFrames;
	}
	
	/**
	 * It sets the bonus for a strike frame of this game
	 * 
	 * @param index The position of the strike frame
	 */
	private void setBonusForStrikeFrame(int index) {
		if(index == (DIMFRAMESOFAGAME - 2)) {
			framesOfAGame[index].setBonus(framesOfAGame[index+1].getFirstThrow() + firstBonusThrow);
		}else if(index == (DIMFRAMESOFAGAME - 1)) {
			framesOfAGame[index].setBonus(firstBonusThrow + secondBonusThrow);
		}else if(framesOfAGame[index+1].isStrike()) {
			framesOfAGame[index].setBonus(framesOfAGame[index+1].getFirstThrow() + framesOfAGame[index+2].getFirstThrow());
		}else {
			framesOfAGame[index].setBonus(framesOfAGame[index+1].getFirstThrow() + framesOfAGame[index+1].getSecondThrow());
		}
	}
	
	/**
	 * It sets the bonus for a spare frame of this game
	 * 
	 * @param index The position of the spare frame
	 */
	private void setBonusForSpareFrame(int index) {
		if(index == (DIMFRAMESOFAGAME - 1)) {
			framesOfAGame[index].setBonus(firstBonusThrow);
		} else {
			framesOfAGame[index].setBonus(framesOfAGame[index+1].getFirstThrow());
		}
	}

}

package tdd.training.bsk;

public class Frame {
	
	private int firstThrow;
	private int secondThrow;
	private int bonus;

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if((firstThrow + secondThrow) < 0 || (firstThrow + secondThrow) > 10)
			throw new BowlingException("Invalid Input, firstThrow + secondThrow must belongs to [0, 10]");
		
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
		bonus = 0;
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		return firstThrow + secondThrow + bonus;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		boolean returnValue = false;
		if(firstThrow == 10) {
			returnValue = true;
		}
		return returnValue;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		boolean returnValue = false;
		if(firstThrow != 10 && (firstThrow + secondThrow)==10) {
			returnValue = true;
		}
		return returnValue;
	}

	/**
	 * It returns true when Object obj is equals to this frame
	 * 
	 * @return <true> if two object are equals, <false> otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frame other = (Frame) obj;
		if (firstThrow != other.firstThrow)
			return false;
		if (secondThrow != other.secondThrow)
			return false;
		return true;
	}

}
